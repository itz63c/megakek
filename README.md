# duo-user duo 2020.1014.61 202010140061 release-keys
- manufacturer: 
- platform: msmnile
- codename: duo
- flavor: qssi-user
- release: 10
- id: hlos-system-user
2020.1014.61
- incremental: 202010140061
- tags: release-keys
- fingerprint: surface/duo/duo:10/2020.1014.61/202010140061:user/release-keys
- brand: surface
- branch: duo-user-duo-2020.1014.61-202010140061-release-keys
- repo: surface_duo_dump
